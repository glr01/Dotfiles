#!/usr/bin/env bash

killall dwmblocks

dunst &
dwmblocks &
setxkbmap br &
nitrogen --restore &
/usr/bin/emacs --daemon &
pulsemixer --set-volume 100 &
picom --config "$HOME/.config/picom/picom.conf" &
